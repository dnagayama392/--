﻿namespace Dentaku
{
    public class NumberOfDigits
    {
        private int numberOfDigits;
        /* 有効桁数(小数点は含まない) */
        private const int PERMISSIBLE_DIGITS = 15;

        public NumberOfDigits()
        {
            Initialize();
        }

        private void Initialize()
        {
            numberOfDigits = 1;
        }

        /// <summary>
        /// 入力値の合計桁数を数え、許容であるかどうかを調べる。
        /// </summary>
        /// <param name="ioData">入出力データ</param>
        /// <returns>許容桁数ならばtrue</returns>
        public bool CheckDigits(IODataStore ioData, NumberField numbers)
        {
            if (IsPermissibleDigits())
            {
                var numberStatus = numbers.GetStatus();
                CountUpNumberOfDigits(ioData, numberStatus);
                return true;
            }
            return false;
        }

        private void CountUpNumberOfDigits(IODataStore ioData, InputState inputStatus)
        {
            if (ioData.IsPushDataPoint())
            {
                return;
            }
            if (ioData.IsPushDataZero() && inputStatus.IsAfterArithmetic())
            {
                return;
            }
            numberOfDigits++;
        }

        private bool IsPermissibleDigits()
        {
            if (numberOfDigits <= PERMISSIBLE_DIGITS)
            {
                return true;
            }
            return false;
        }

        public void Reset()
        {
            Initialize();
        }
    }
}

﻿using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Dentaku
{
    /// <summary>
    /// stringを保持する内部データ
    /// </summary>
    public class InsideData
    {
        private string strDatum;

        public InsideData(string str)
        {
            strDatum = str;
        }

        public static InsideData operator +(string str, InsideData inpData)
        {
            return new InsideData(str + inpData.strDatum);
        }

        public static InsideData operator +(InsideData inpDataA, InsideData inpDataB)
        {
            return new InsideData(inpDataA.strDatum + inpDataB.strDatum);
        }

        public override string ToString()
        {
            return strDatum;
        }

        /// <summary>
        /// 保持する値をDouble型にして返す(変換不可ならNaN)
        /// </summary>
        /// <returns>Double型に変換した値</returns>
        public double ToDouble()
        {
            if (IsDoubleTryParse())
            {
                return double.Parse(strDatum);
            }
            Debug.Assert(false, "strDatum can't change to Double.");
            return double.NaN;
        }

        public bool IsUsingButtonData()
        {
            if (IsNumber())
            {
                return true;
            }
            return IsArithmeticSign();
        }

        public bool IsNumber()
        {
            if (IsNaturalNumber())
            {
                return true;
            }
            return IsPoint();
        }

        public bool IsNaturalNumber()
        {
            Regex naturalNumber = new Regex(@"\d");
            if (naturalNumber.IsMatch(strDatum))
            {
                return true;
            }
            return false;
        }

        public bool IsZero()
        {
            var datum = ToDouble();
            if (datum.ToString() == Constant.ZERO)
            {
                return true;
            }
            return false;
        }

        public bool IsPoint()
        {
            if (strDatum == Constant.POINT)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// "", "+", "-","*", "/"のいずれかにマッチするか
        /// </summary>
        /// <returns>マッチしたらtrue</returns>
        public bool IsUsingSignData()
        {
            if (strDatum == Constant.EMPTY)
            {
                return true;
            }
            return IsArithmeticSign();
        }

        private bool IsArithmeticSign()
        {
            Regex arithmeticSymbol = new Regex(@"\+|-|\*|/");
            if (arithmeticSymbol.IsMatch(strDatum))
            {
                return true;
            }
            return IsEqual();
        }

        public bool IsEqual()
        {
            if (strDatum == Constant.EQUAL)
            {
                return true;
            }
            return false;
        }

        public bool IsDoubleTryParse()
        {
            double notused = double.NaN;
            return double.TryParse(strDatum, out notused);
        }
    }
}

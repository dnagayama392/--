﻿using System;
using System.Text.RegularExpressions;

namespace Dentaku
{
    /// <summary>
    /// 入出力データ
    /// </summary>
    public class IODataStore
    {
        private InsideData pushButton; // 入力された文字
        private InsideData viewNumber; // 出力する文字列

        public InsideData PushButton
        {
            get
            {
                return pushButton;
            }
            set
            {
                if (value.IsUsingButtonData())
                {
                    pushButton = value;
                    return;
                }
                throw new ArgumentException();
            }
        }

        public InsideData ViewNumber
        {
            get
            {
                return viewNumber;
            }
            set
            {
                if (value.IsDoubleTryParse())
                {
                    viewNumber = value;
                    return;
                }
                throw new ArgumentException();
            }
        }

        public IODataStore()
        {
            Initialize();
        }

        private void Initialize()
        {
            PushButton = new InsideData(Constant.ZERO);
            ClearViewNumber();
        }

        public void ClearViewNumber()
        {
            ViewNumber = new InsideData(Constant.ZERO);
        }

        /// <summary>
        /// 入力が数字[0-9]かどうか確認する。
        /// </summary>
        /// <returns>入力が数字ならばtrue</returns>
        public bool IsPushDataNuturalNumber()
        {
            return PushButton.IsNaturalNumber();
        }

        public bool IsPushDataZero()
        {
            return PushButton.IsZero();
        }

        public bool IsPushDataPoint()
        {
            return PushButton.IsPoint();
        }

        public void ClearAll()
        {
            Initialize();
        }
    }
}

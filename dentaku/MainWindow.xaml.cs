﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Dentaku
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private IODataStore ioData;
        private Calculation calculator;

        public MainWindow()
        {
            InitializeComponent();
            calculator = new Calculation();
            ioData     = new IODataStore();
        }

        private void buttonNum_Click(object sender, RoutedEventArgs e)
        {
            var buttonContent = (string)((Button)sender).Content;
            ioData.PushButton = new InsideData(buttonContent);
            try
            {
                calculator.ExecuteNumberInputProcessing(ioData);
            }
            catch (ArgumentException)
            {
                ShowErrorArgumentException();
                return;
            }
            var viewNumber = ioData.ViewNumber;
            labelViewArea.Content = viewNumber.ToString();
        }

        private void buttonSign_Click(object sender, RoutedEventArgs e)
        {
            var buttonContent = (string)((Button)sender).Content;
            ioData.PushButton = new InsideData(buttonContent);
            try
            {
                calculator.ExecuteArithmeticProcessing(ioData);
            }
            catch (DivideByZeroException)
            {
                ShowErrorDivideByZeroException();
                return;
            }
            catch (ArgumentException)
            {
                ShowErrorArgumentException();
                return;
            }
            var viewNumber = ioData.ViewNumber;
            labelViewArea.Content = viewNumber.ToString();
            SetLabelSign(ioData.PushButton);
        }

        /// <summary>
        /// 符号を数字の左に表示するためのメソッド
        /// </summary>
        /// <param name="pushButton">入力符号</param>
        private void SetLabelSign(InsideData pushButton)
        {
            if (pushButton.IsEqual())
            {
                labelSign.Content = Constant.EMPTY;
                return;
            }
            labelSign.Content = pushButton.ToString();
        }

        private void buttonClear_Click(object sender, RoutedEventArgs e)
        {
            labelViewArea.Content = Constant.ZERO;
            var buttonContent = (string)((Button)sender).Content;
            if (buttonContent == Constant.ALLCLEAR)
            {
                ClearAll();
                return;
            }
            ClearEntry();
        }

        private void ClearAll()
        {
            labelSign.Content = Constant.EMPTY;
            calculator.ClearAll();
            ioData.ClearAll();
        }

        private void ClearEntry()
        {
            calculator.ClearEntry();
            ioData.ClearViewNumber();
        }

        /// <summary>
        /// 入力値が不正であることをメッセージボックスで表示する
        /// </summary>
        private void ShowErrorArgumentException()
        {
            MessageBox.Show("入力値が不正です。\r\n正しい値を入力してください。");
        }

        /// <summary>
        /// ゼロ除算であったことをメッセージボックスで表示する
        /// </summary>
        private void ShowErrorDivideByZeroException()
        {
            MessageBox.Show("0で除算しようとしました。\r\n[AC]を押して再計算してください。");
        }
    }
}

﻿using System;

namespace Dentaku
{
    /// <summary>
    /// 入力された演算子を扱うクラス
    /// </summary>
    public class ArithmeticSigns
    {
        private InsideData previous;
        private InsideData pushed;

        public ArithmeticSigns()
        {
            Initialize();
        }

        private void Initialize()
        {
            previous = new InsideData(Constant.EMPTY);
            pushed   = new InsideData(Constant.PLUS);
        }

        public InsideData GetPreviousSign()
        {
            return previous;
        }

        public void SetPushSign(InsideData inputData)
        {
            SetSign(inputData, ref pushed);
        }

        private void SetSign(InsideData strData, ref InsideData sign)
        {
            if (strData.IsUsingSignData())
            {
                sign = strData;
                return;
            }
            throw new ArgumentException();
        }

        public void SetPreviousPlus()
        {
            previous = new InsideData(Constant.PLUS);
        }

        public void SetPushToPrevious()
        {
            previous = pushed;
        }

        public bool IsPushEqual()
        {
            return pushed.IsEqual();
        }
    }
}

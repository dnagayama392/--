﻿using System;
using System.Diagnostics;

namespace Dentaku
{
    /// <summary>
    /// 計算用の数字を文字列型で格納
    /// </summary>
    public class StoredNumber
    {
        private InsideData strTotal; // 計算結果の値
        private InsideData strEntry; // 入力された値

        public InsideData StrTotal
        {
            get
            {
                return strTotal;
            }
            private set
            {
                if (!SetStrNumber(value, ref strTotal))
                {
                    Debug.Assert(false, "Invalid input to StrTotal.");
                }
            }
        }

        public InsideData StrEntry
        {
            get
            {
                return strEntry;
            }
            set
            {
                if (!SetStrNumber(value, ref strEntry))
                {
                    Debug.Assert(false, "Invalid input to StrEntry.");
                }
            }
        }

        public StoredNumber()
        {
            StrTotal = new InsideData(Constant.ZERO);
            StrEntry = new InsideData(Constant.ZERO);
        }

        private bool SetStrNumber(InsideData inpDataNumber, ref InsideData strData)
        {
            // Doubleに変換できる値ならば入力
            if (inpDataNumber.IsDoubleTryParse())
            {
                strData = inpDataNumber;
                return true;
            }
            return false;
        }

        public void SumUp()
        {
            StrTotal = new InsideData((GetDoubleTotal() + GetDoubleEntry()).ToString());
        }

        public void Subtract()
        {
            StrTotal = new InsideData((GetDoubleTotal() - GetDoubleEntry()).ToString());
        }

        public void Multiply()
        {
            StrTotal = new InsideData((GetDoubleTotal() * GetDoubleEntry()).ToString());
        }

        public void Divide()
        {
            // ゼロ除算の例外処理
            //double notused = double.NaN;
            //Debug.Assert(double.TryParse(Total, out notused));
            if (IsDivideZero())
            {
                Debug.Assert(false, "Divide By Zero Exception.");
                throw new DivideByZeroException();
            }
            StrTotal = new InsideData((GetDoubleTotal() / GetDoubleEntry()).ToString());
        }

        private bool IsDivideZero()
        {
            return StrEntry.IsZero();
        }

        private double GetDoubleTotal()
        {
            return StrTotal.ToDouble();
        }

        private double GetDoubleEntry()
        {
            return StrEntry.ToDouble();
        }

        public void SetEntryZero()
        {
            StrEntry = new InsideData(Constant.ZERO);
        }

        public void SetTotalToEntry()
        {
            StrEntry = StrTotal;
        }

    }
}

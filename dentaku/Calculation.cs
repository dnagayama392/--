﻿using System;

namespace Dentaku
{
    /// <summary>
    /// 入力に対して大まかな処理を指示するクラス
    /// </summary>
    public class Calculation
    {
        private NumberField numbers;
        private ArithmeticAction arithmetics;
        private NumberOfDigits numberOfDigits;

        public Calculation()
        {
            Initialize();
        }

        private void Initialize()
        {
            numbers        = new NumberField();
            arithmetics    = new ArithmeticAction();
            numberOfDigits = new NumberOfDigits();
        }

        /// <summary>
        /// 数値が入力された時に呼び出します
        /// </summary>
        /// <param name="IODatas">入出力情報</param>
        public void ExecuteNumberInputProcessing(IODataStore IODatas)
        {
            if (!numberOfDigits.CheckDigits(IODatas, numbers))
            {
                return;
            }
            /* 等号の直後ならば初期化 */
            if (arithmetics.IsStateAfterEqual())
            {
                Initialize();
            }
            numbers.ExecuteNumberInputProcessing(IODatas);
        }

        /// <summary>
        /// 算術記号が入力された時に呼び出します
        /// </summary>
        /// <param name="IODatas">入出力情報</param>
        public void ExecuteArithmeticProcessing(IODataStore IODatas)
        {
            arithmetics.ExecuteArithmeticProcessing(numbers, IODatas);
            numberOfDigits.Reset();
        }

        /// <summary>
        /// 最後に入力した数値をクリア
        /// </summary>
        public void ClearEntry()
        {
            numbers.ClearEntry();
            numberOfDigits.Reset();
        }

        public void ClearAll()
        {
            Initialize();
        }
    }
}

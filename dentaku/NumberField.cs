﻿
namespace Dentaku
{
    /// <summary>
    /// 入力された数、演算結果と数の状態を扱うクラス
    /// </summary>
    public class NumberField
    {
        private StoredNumber storedNumbers;
        private InputState inputStatus;

        public NumberField()
        {
            Initialize();
        }

        private void Initialize()
        {
            storedNumbers = new StoredNumber();
            inputStatus   = new InputState();
        }

        public InputState GetStatus()
        {
            return inputStatus;
        }
        
        public void ExecuteNumberInputProcessing(IODataStore ioData)
        {
            ExecuteProcessPushedNumberButton(ioData);
            ioData.ViewNumber = storedNumbers.StrEntry;
        }

        /// <summary>
        /// 押されたボタンに対して振る舞いを決定し、実行
        /// </summary>
        /// <param name="ioData">入出力情報</param>
        private void ExecuteProcessPushedNumberButton(IODataStore ioData)
        {
            if (inputStatus.IsAfterArithmetic())
            {
                ExecuteProcessAfterArithmetic(ioData);
                return;
            }
            ExecuteProcessAfterNumber(ioData);
        }

        /// <summary>
        /// 一つ前に数字が押された場合の処理
        /// </summary>
        /// <param name="ioData">入出力情報</param>
        private void ExecuteProcessAfterNumber(IODataStore ioData)
        {
            if (ioData.IsPushDataNuturalNumber())
            {
                storedNumbers.StrEntry += ioData.PushButton;
                return;
            }
            if (!inputStatus.IsUsingDecimalPoint())
            {
                storedNumbers.StrEntry += ioData.PushButton;
                inputStatus.SetDecimalPointUseState();
            }
        }

        /// <summary>
        /// 一つ前に符号が押されたときの処理
        /// </summary>
        /// <param name="ioData">入出力情報</param>
        private void ExecuteProcessAfterArithmetic(IODataStore ioData)
        {
            if (ioData.IsPushDataPoint())
            {
                storedNumbers.StrEntry = Constant.ZERO + ioData.PushButton;
                inputStatus.SetNumberInputState();
                inputStatus.SetDecimalPointUseState();
                return;
            }
            if (ioData.IsPushDataZero())
            {
                storedNumbers.StrEntry = ioData.PushButton;
                return;
            }
            storedNumbers.StrEntry = ioData.PushButton;
            inputStatus.SetNumberInputState();
        }

        public void ClearEntry()
        {
            storedNumbers.SetEntryZero();
            inputStatus.Reset();
        }

        public void ClearAll()
        {
            Initialize();
        }

        public void SumUp()
        {
            storedNumbers.SumUp();
        }

        public void Subtract()
        {
            storedNumbers.Subtract();
        }

        public void Multiply()
        {
            storedNumbers.Multiply();
        }

        public void Divide()
        {
            storedNumbers.Divide();
        }

        public void SetStatusAfrerArithemtic(IODataStore ioData)
        {
            inputStatus.Reset();
            ioData.ViewNumber = storedNumbers.StrTotal;
        }

        public void SetEntryZero()
        {
            storedNumbers.SetEntryZero();
        }

        public void SetTotalToEntry()
        {
            storedNumbers.SetTotalToEntry();
        }

        public bool IsStateAfterArithmetic()
        {
            return inputStatus.IsAfterArithmetic();
        }
    }
}

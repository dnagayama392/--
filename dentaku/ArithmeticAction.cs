﻿using System;

namespace Dentaku
{
    /// <summary>
    /// 入力された演算子の振る舞いを扱うクラス
    /// </summary>
    public class ArithmeticAction
    {
        private ArithmeticSigns signs;
        private bool stateAfterEqual;

        public ArithmeticAction()
        {
            Initialize();
        }

        private void Initialize()
        {
            signs = new ArithmeticSigns();
            stateAfterEqual = false;
        }

        public bool IsStateAfterEqual()
        {
            return stateAfterEqual;
        }

        /// <summary>
        /// 符号を押された場合の処理
        /// </summary>
        /// <param name="numbers">内部数字情報</param>
        /// <param name="ioData">入出力情報</param>
        public void ExecuteArithmeticProcessing(NumberField numbers, IODataStore ioData)
        {
            signs.SetPushSign(ioData.PushButton);
            ExecuteProcessBySign(numbers);
            numbers.SetStatusAfrerArithemtic(ioData);
        }

        private void ExecuteProcessBySign(NumberField numbers)
        {
            if (signs.IsPushEqual())
            {
                ExecuteCaseEqual(numbers);
                return;
            }
            ExecuteCaseArithmeticSymbol(numbers);
        }

        private void ExecuteCaseEqual(NumberField numbers)
        {
            CalculateByPreviousSign(numbers);
            stateAfterEqual = true;
        }

        /// <summary>
        /// 一つ前の算術符号に応じて演算処理
        /// </summary>
        /// <param name="numbers">内部数字情報</param>
        private void CalculateByPreviousSign(NumberField numbers)
        {
            InsideData previousSign = signs.GetPreviousSign();
            switch (previousSign.ToString())
            {
                case Constant.EMPTY:
                    ExecuteCaseEmpty(numbers);
                    break;
                case Constant.PLUS:
                    SumUp(numbers);
                    break;
                case Constant.MINUS:
                    Subtract(numbers);
                    break;
                case Constant.ASTERISK:
                    Multiply(numbers);
                    break;
                case Constant.SLASH:
                    Divide(numbers);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// +, -, *, / のいずれかが押された場合の処理
        /// </summary>
        /// <param name="numbers">内部数字情報</param>
        private void ExecuteCaseArithmeticSymbol(NumberField numbers)
        {
            ExecuteProcessAfterEqual(numbers);
            if (numbers.IsStateAfterArithmetic())
            {
                signs.SetPushToPrevious();
                return;
            }
            CalculateByPreviousSign(numbers);
            signs.SetPushToPrevious();
        }

        /// <summary>
        /// 演算記号を押す一つ前に=(等号)が押されていたか判定して処理
        /// </summary>
        /// <param name="numbers">内部数字情報</param>
        private void ExecuteProcessAfterEqual(NumberField numbers)
        {
            if (stateAfterEqual)
            {
                numbers.SetTotalToEntry();
                signs.SetPreviousPlus();
            }
            stateAfterEqual = false;
        }

        /// <summary>
        /// 初期化後初めて符号ボタンを押したときの演算
        /// </summary>
        /// <param name="numbers">内部数字情報</param>
        private void ExecuteCaseEmpty(NumberField numbers)
        {
            numbers.SumUp();
            if (signs.IsPushEqual())
            {
                numbers.SetEntryZero();
            }
        }

        private void SumUp(NumberField numbers)
        {
            // 委譲
            numbers.SumUp();
        }

        private void Subtract(NumberField numbers)
        {
            numbers.Subtract();
        }

        private void Multiply(NumberField numbers)
        {
            numbers.Multiply();
        }

        private void Divide(NumberField numbers)
        {
            numbers.Divide();
        }

        public void ClearAll()
        {
            Initialize();
        }
    }
}

﻿namespace Dentaku
{
    public class InputState
    {
        private bool stateAfterArithmetic;
        private bool stateUsingDecimalPoint;

        public InputState()
        {
            Initialize();
        }

        private void Initialize()
        {
            stateAfterArithmetic   = true;
            stateUsingDecimalPoint = false;
        }

        public void SetNumberInputState()
        {
            stateAfterArithmetic = false;
        }

        public void SetDecimalPointUseState()
        {
            stateUsingDecimalPoint = true;
        }

        public bool IsUsingDecimalPoint()
        {
            return stateUsingDecimalPoint;
        }

        public bool IsAfterArithmetic()
        {
            return stateAfterArithmetic;
        }

        public void Reset()
        {
            Initialize();
        }
    }
}

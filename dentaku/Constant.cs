﻿namespace Dentaku
{
    /// <summary>
    /// c
    /// </summary>
    struct Constant
    {
        public const string ZERO     = "0";
        public const string POINT    = ".";
        public const string PLUS     = "+";
        public const string MINUS    = "-";
        public const string ASTERISK = "*";
        public const string SLASH    = "/";
        public const string EQUAL    = "=";
        public const string EMPTY    = "";
        public const string ALLCLEAR = "AC";
    }
}
